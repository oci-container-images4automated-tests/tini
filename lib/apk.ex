defmodule Buildah.Apk.Tini do

    alias Buildah.{Apk, Cmd} # Print

    @tini_cmd "tini"

    def tini_cmd() do @tini_cmd end

    def on_container(container, options) do
        Apk.packages_no_cache(container, [
            # "--repository", "https://dl-cdn.alpinelinux.org/alpine/edge/community", # Was testing, now community
            "tini"
        ], options)
        {tini_exec_, 0} = Cmd.run(
            container,
            ["sh", "-c", "command -v " <> @tini_cmd]
        )
        {_, 0} = Cmd.config(
            container,
            entrypoint: "[\"#{String.trim(tini_exec_)}\", \"--\"]"
        )
    end

    def test(container, image_ID, options) do
        {_, 0} = Cmd.run(container, [@tini_cmd, "--version"], into: IO.stream(:stdio, :line))
        {_, 0} = Podman.Cmd.run(image_ID, [@tini_cmd, "--version"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        Apk.packages_no_cache(container, ["psmisc"], options)
        {_, 0} = Cmd.run(container, ["whoami"], into: IO.stream(:stdio, :line))
        {_, 0} = Cmd.run(container, ["pstree"], into: IO.stream(:stdio, :line))
        {_, 0} = Podman.Cmd.run(image_ID, ["whoami"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
        {_, 0} = Podman.Cmd.run(image_ID, ["pstree"],
            tty: true, rm: true, into: IO.stream(:stdio, :line)
        )
    end

end

